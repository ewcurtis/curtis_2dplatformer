﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnCoins : MonoBehaviour
{

    public Transform[] spawnCoins;
    public GameObject coin;
    // Start is called before the first frame update
    void Start()
    {

        Spawn();

    }

   
    void Spawn()
    {
        for (int i = 0; i < spawnCoins.Length; i++) {
            int coinFlip = Random.Range(0, 2);

            if (coinFlip > 0)
            {
                Instantiate(coin, spawnCoins[i].position, Quaternion.identity);
            }
        }
        
    }
}
